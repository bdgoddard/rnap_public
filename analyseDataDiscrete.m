function dataOut = analyseDataDiscrete(data,name,doAnalysePlots)

    set(0,'DefaultLineLineWidth',2); set(0,'DefaultAxesFontSize',20);

    tFull = data(1).tFull;
    nTimes = length(tFull);
    nRuns = length(data);

    edges = 0:1:7000;
    mids = edges + edges(1)/2;
    mids = mids(1:end-1);

    dx = edges(2);

    tPos = 1;
    
    rhoFull = zeros(size(mids));
    dxAvgFull = zeros(size(mids));
    dxAvg70Full = zeros(size(mids));
    structureFull = zeros(size(mids));
    hybridFull = zeros(size(mids));
    windingFull = zeros(size(mids));
    backtrackingFull = zeros(size(mids));
    collisionsFull = zeros(size(mids));
    xEndFull = [];
    nBins = length(mids);

    for iRun = 1:nRuns

        xFull = data(iRun).xFull;
        dxAvg = data(iRun).dxAvgFull;
        structure = data(iRun).structureFull;
        hybrid = data(iRun).hybridFull;
        winding = data(iRun).windingFull;
        backtracking = data(iRun).backtrackingFull;    
        collisions = data(iRun).collisionsFull;  
        xEnd = data(iRun).xEnd;
        
        for iTime = tPos:nTimes
            
            % positional data
            x = xFull(:,iTime);
            killMask = (x>7000) | isnan(x);
            x(killMask) = [];
            xBoxes = floor( nBins* x / 7000 ) + 1;
            xBoxes(xBoxes==nBins+1)=nBins;
                        
            % number of particles
            rho = ( accumarray(xBoxes, ones(size(x))) ).'; %Matlab trick to make a histogram
            rhoFull(1:length(rho)) = rhoFull(1:length(rho)) + rho; % full histogram
            
            % average separation
            dxAvgT = dxAvg(:,iTime);
            dxAvgT(killMask) = [];
            dxAvgHist = accumarray(xBoxes, dxAvgT).'; 
            dxAvgFull(1:length(dxAvgHist)) = dxAvgFull(1:length(dxAvgHist)) + dxAvgHist;
            
            % count the number with average separation less than 70
            dxAvg70Hist = accumarray(xBoxes, (dxAvgT<70)).'; 
            dxAvg70Full(1:length(dxAvgHist)) = dxAvg70Full(1:length(dxAvgHist)) + dxAvg70Hist;
            
            % forces
            structureT = structure(:,iTime);
            structureT(killMask) = [];
            structureHist = accumarray(xBoxes, structureT).'; 
            structureFull(1:length(structureHist)) = structureFull(1:length(structureHist)) + structureHist;

            hybridT = hybrid(:,iTime);
            hybridT(killMask) = [];
            hybridHist = accumarray(xBoxes, hybridT).'; 
            hybridFull(1:length(hybridHist)) = hybridFull(1:length(hybridHist)) + hybridHist;

            windingT = winding(:,iTime);
            windingT(killMask) = [];
            windingHist = accumarray(xBoxes, windingT).'; 
            windingFull(1:length(windingHist)) = windingFull(1:length(windingHist)) + windingHist;

            % backtracking
            backtrackingT = backtracking(:,iTime);
            backtrackingT(killMask) = [];
            backtrackingHist = accumarray(xBoxes, backtrackingT).'; 
            backtrackingFull(1:length(backtrackingHist)) = backtrackingFull(1:length(backtrackingHist)) + backtrackingHist;

            % collisions
            collisionsT = collisions(:,iTime);
            collisionsT(killMask) = [];
            collisionsHist = accumarray(xBoxes, collisionsT).'; 
            collisionsFull(1:length(collisionsHist)) = collisionsFull(1:length(collisionsHist)) + collisionsHist;

            
        end

        xEndFull = [xEndFull; xEnd];
        
    end

    % average separation
    dxAvgFull = dxAvgFull./rhoFull;
    
%     % mean average separation over whole domain
%     dxAvgCut = dxAvgFull(~isnan(dxAvgFull)); % strip off end
%     dxAvgCut(isinf(dxAvgCut)) = []; % remove anywhere where the density is zero
%     dxAvgMean = mean(dxAvgCut);

    % probability within 70
    dxAvg70Full = dxAvg70Full./rhoFull;
    
    % forces
    structureFull = structureFull./rhoFull;
    hybridFull = hybridFull./rhoFull;
    windingFull = windingFull./rhoFull;
    
    % backtracking
    backtrackingFullRaw = backtrackingFull;
    backtrackingFull = backtrackingFull./rhoFull;
    
    % collisions
    collisionsFullRaw = collisionsFull;
    collisionsFull = collisionsFull./rhoFull;
    
    % normalise density
    rhoFull = rhoFull/(nTimes-tPos+1)/dx/nRuns;
    nParticles = sum(rhoFull*dx);
    rhoFull = rhoFull/nParticles;
   
    % output csv
    csvData = [mids; rhoFull; dxAvgFull; dxAvg70Full; structureFull; hybridFull; windingFull; backtrackingFull; backtrackingFullRaw; collisionsFull; collisionsFullRaw];
%     csvFile = ['Data' filesep 'nParticles' num2str(nParticles) name '.csv'];
    csvFile = ['Data' filesep 'nParticles' num2str(nParticles) '_' name '.csv'];
    csvwrite(csvFile,csvData);
    
    % output data
    dataOut.mids = mids;
    dataOut.rhoFull = rhoFull; 
    dataOut.dxAvgFull = dxAvgFull; 
    dataOut.dxAvg70Full = dxAvg70Full; 
    dataOut.structureFull = structureFull; 
    dataOut.hybridFull = hybridFull; 
    dataOut.windingFull = windingFull; 
    dataOut.backtrackingFull = backtrackingFull;
    dataOut.backtrackingFullRaw = backtrackingFullRaw;
    dataOut.collisionsFull = collisionsFull;
    dataOut.collisionsFullRaw = collisionsFullRaw;
    dataOut.nParticles = nParticles;
    dataOut.xEnd = xEndFull;
    
    if(doAnalysePlots)
        hf1 = figure('Position',[100,100,1200,800]);
        plot(mids,rhoFull);
        xlabel('Space');
        ylabel('Density');
        xlim([0,7000]);
        
        hf2 = figure('Position',[100,100,1200,800]);
        plot(mids,dxAvgFull);
        xlabel('Space');
        ylabel('Average Separation');
        xlim([0,7000]);

        hf3 = figure('Position',[100,100,1200,800]);
        plot(mids,dxAvg70Full);
        xlabel('Space');
        ylabel('Probability Within 70');
        xlim([0,7000]);

        hf4 = figure('Position',[100,100,1200,800]);
        plot(mids,structureFull,'r');
        hold on
        plot(mids,hybridFull,'b');
        plot(mids,windingFull,'m');
        xlabel('Space');
        ylabel('Forces');
        xlim([0,7000]);
        legend({'Structure', 'Hybrid', 'Winding'});

        hf5 = figure('Position',[100,100,1200,800]);
        plot(mids,backtrackingFull);
        xlabel('Space');
        ylabel('Backtracking');
        xlim([0,7000]);

        hf5 = figure('Position',[100,100,1200,800]);
        plot(mids,collisionsFull);
        xlabel('Space');
        ylabel('Collisions');
        xlim([0,7000]);
        
        shg
    end
    
end